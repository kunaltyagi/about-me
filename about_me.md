# About Me

## Personal Details

* __Name__: Kunal Tyagi
* __Phone number__: (+91) 7738874289
* __E mail ID__: tyagi.kunal@hotmail.com
* __IRC__: matrixMinion
* __TimeZone__: UTC +5:30
* __Favourite Platform__: C++ on Ubuntu
* __University__: Indian Institute of Technology, Bombay
* __Degree__: B.Tech in Aerospace(Hons.) with Computer Science
* __Expected Graduation Date__: May 2016
* __Interests__: robotics, systems control and navigation, simulation,
avionics, fluid dynamics, hypersonic flow, shock wave interactions, propulsion,
deep neural networks, image processing, cognition, natural language processing, genetic algorithms, data mining

## Course Work and Experience

### Robotics

* [Course] Introduction to Engineering Design
* Created robots like
    * Matsya series of [AUV-IITB] _\#TeamProject_
    * Automatic Folding Bus Seat to increase space for standee passengers _\#TeamProject_
    * Autonomous Grid Solver
    * Line Follower
    * Hexapod _\#TeamProject_
    * Self-Balancing 2 wheeled robot
    * Multi-stage Water Rocket _\#TeamProject_
    * RC Acrobatic Plane _\#TeamProject_
    * RC Monster Truck

### Software Engineering

* [Course] Introduction to Computer Science
* [Course] Discrete Structures
* [Course] Data Structures and Algorithms
* Worked on several projects like
    * [AUV-IITB]'s software stack: Worked on controls, motion planning, simulation and navigation _\#TeamProject_
    * Driver for DVL, a port from RoC to ROS _\#TeamProject_
    * Genetic Algorithm for custom airfoil shape
    * My own toy shell and language: Bash++ (Bash with classes)
    * Parser for a scripting language _\#TeamProject_
    * Webpages with heavy use of Javascripting, CSS3, HTML5 and PHP
    * Website for Hostel 6, IIT Bombay (on [github])
    * Several projects in LaTeX, Ruby, Python. Find more about them on [github]

### Computer Graphics

* Feature detection for given shapes in different colors and lightening conditions

### Physics Simulation

* N body Simulator _\#TeamProject_
* Gazebo simulator for [AUV-IITB] _\#TeamProject_
* Airfoil analyzer
* Simulation of Energy consumption of Active Flow Control Systems

## Experience

### Sample Code

### Specialized skills

### References

* Prashant Iyengar
* S K Savant
* Nilesh Kulkarni
* Anay Kumar Joshi
* Prof. A M Pradeep
* Prof. Arya Sinha
* Prof. Leena Vachhani

## Statement of Intent

Interests and background

Please tell us which of the project ideas you are interested in and why you’d like to work on it. If you have a proposal for a project not included on our list, please describe the idea clearly and provide a motivation for the work and a timeline for how you plan to accomplish it.

## Project Summary

One Line

## Project Description

Detailed description

## Rough Roadmap

Timeline here, with milestones, deliverables

## Availability

Competition during \_\_ to \_\_ and classes from \_\_ to \_\_

## Patch details

Submit some patched dude

[github]: http://github.com/kunaltyagi
[AUV-IITB]: http://www.auv-iitb.org
